package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOffer;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferExistException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("container")
public class OfferServiceDoNotSaveDuplicateOfferWithContainerTests implements SampleOffer, SampleOfferDto {

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_save_offers_to_database(@Autowired OfferService offerService) {
        //GIVEN
        List<OfferDto> offerDtos = Arrays.asList(firstOfferDtoWithoutId(), secondOfferDtoWithoutId());
        int numberOffersBeforeSave = offerService.getOffers().size();
        //WHEN
        List<OfferDto> savedOffers = offerService.saveAll(offerDtos);
        int numberOffersAfterSave = offerService.getOffers().size();
        //THEN
        assertThat(numberOffersAfterSave).isGreaterThan(numberOffersBeforeSave);
        assertThat(savedOffers).containsAnyElementsOf(offerDtos);
    }

    @Test
    public void should_throw_OfferExistException_when_the_offer_is_duplicate(@Autowired OfferService offerService) {
        //GIVEN
        offerService.createOrUpdateOffer(offerDtoWithNotUniqueUrl());
        int numberOffersBeforeSave = offerService.getOffers().size();
        OfferDto offerWithNotUniqueURL = offerDtoWithNotUniqueUrl();
        //WHEN //THEN
        Assertions.assertThrows(OfferExistException.class, () -> offerService.createOrUpdateOffer(offerWithNotUniqueURL));
        int numberOffersAfterSave = offerService.getOffers().size();
        assertThat(numberOffersAfterSave).isEqualTo(numberOffersBeforeSave);
    }
}
