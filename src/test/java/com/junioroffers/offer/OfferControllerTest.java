package com.junioroffers.offer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.config.MockMvcConfig;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferErrorResponse;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = MockMvcConfig.class)
public class OfferControllerTest implements SampleOfferDto {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void should_return_list_with_offer() throws Exception {
        //given
        List<OfferDto> offers = Lists.newArrayList(firstOfferDtoWithId(), secondOfferDtoWithId());
        String exceptedResponse = objectMapper.writeValueAsString(offers);
        //when
        MvcResult mvcResult = mockMvc.perform(get("/offers")).andExpect(status().isOk()).andReturn();
        String receivedResponse = mvcResult.getResponse().getContentAsString();
        //then
        assertThat(receivedResponse).isEqualTo(exceptedResponse);
    }

    @Test
    public void should_return_status_ok_when_get_for_offer_with_given_id() throws Exception {
        //given
        OfferDto secondOffer = secondOfferDtoWithId();
        String expectedResponse = objectMapper.writeValueAsString(secondOffer);
        String offerId = "629f77e9645fea56b0d31843";
        //when
        MvcResult mvcResult = mockMvc.perform(get("/offers/" + offerId)).andExpect(status().isOk()).andReturn();
        String receivedResponse = mvcResult.getResponse().getContentAsString();
        //then
        assertThat(receivedResponse).isEqualTo(expectedResponse);
    }

    @Test
    public void should_return_status_not_found_when_there_is_not_offer_with_given_id_while_get_method() throws Exception {
        //given
        int offerId = 500;
        OfferErrorResponse offerErrorResponse = OfferErrorResponse.builder()
                .message("There is no offer with id: " + offerId)
                .timestamp(LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)))
                .httpStatus(HttpStatus.NOT_FOUND)
                .build();
        String errorResponse = objectMapper.writeValueAsString(offerErrorResponse);
        //when
        MvcResult mvcResult = mockMvc.perform(get("/offers/" + offerId)).andExpect(status().isNotFound()).andReturn();
        String receivedResponse = mvcResult.getResponse().getContentAsString();
        //then
        assertThat(receivedResponse).isEqualTo(errorResponse);
    }

    @Test
    public void should_return_status_ok_when_offer_is_updated() throws Exception {
        //given
        String endpoint = "/offers";
        String updatedOffer = objectMapper.writeValueAsString(firstOfferDtoWithId());
        //when
        MvcResult mvcResult = mockMvc.perform(postMethodWithWithObjectAsJSON(endpoint, updatedOffer))
                .andExpect(status().isOk()).andReturn();
        String receivedResponse = mvcResult.getResponse().getContentAsString();
        //then
        assertThat(receivedResponse).isEqualTo(updatedOffer);
    }

    @Test
    public void should_return_status_created_when_new_offer_is_created() throws Exception {
        //given
        String endpoint = "/offers";
        String createdOffer = objectMapper.writeValueAsString(firstOfferDtoWithoutId());
        //when
        MvcResult mvcResult = mockMvc.perform(postMethodWithWithObjectAsJSON(endpoint, createdOffer))
                .andExpect(status().isCreated()).andReturn();
        String receivedResponse = mvcResult.getResponse().getContentAsString();
        //then
        assertThat(receivedResponse).isEqualTo(createdOffer);
    }

    @Test
    public void should_return_status_ok_when_offer_is_successful_deleted() throws Exception {
        //given
        String offerId = "629f77e9645fea56b0d31842";
        //when
        MvcResult mvcResult = mockMvc.perform(delete("/offers/" + offerId)).andExpect(status().isOk()).andReturn();
        int responseStatus = mvcResult.getResponse().getStatus();
        //then
        assertThat(responseStatus).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void should_return_status_not_modified_when_there_is_no_offer_with_given_id_while_delete_method() throws Exception {
        //given
        int offerId = 500;
        //when
        MvcResult mvcResult = mockMvc.perform(delete("/offers/" + offerId)).andExpect(status().isNotModified()).andReturn();
        int responseStatus = mvcResult.getResponse().getStatus();
        //then
        assertThat(responseStatus).isEqualTo(HttpStatus.NOT_MODIFIED.value());
    }

    @Test
    public void should_return_status_conflict_when_try_to_save_offer_with_existing_url() throws Exception {
        //GIVEN
        String endpoint = "/offers";
        String notUniqueOffer = objectMapper.writeValueAsString(offerDtoWithNotUniqueUrl());
        //WHEN
        MvcResult mvcResult = mockMvc.perform(postMethodWithWithObjectAsJSON(endpoint, notUniqueOffer))
                .andExpect(status().isConflict()).andReturn();
        int responseStatus = mvcResult.getResponse().getStatus();
        //THEN
        assertThat(responseStatus).isEqualTo(HttpStatus.CONFLICT.value());
    }

    @Test
    public void should_return_status_bad_request_when_is_issue_with_validation_object() throws Exception {
        //GIVEN
        String endpoint = "/offers";
        String objectWithEmptyCompanyName = objectMapper.writeValueAsString(offerDtoWithEmptyCompanyName());
        String wrongValidationMessage = "Company name cannot be empty.";
        //WHEN
        MvcResult mvcResult = mockMvc.perform(postMethodWithWithObjectAsJSON(endpoint, objectWithEmptyCompanyName))
                .andExpect(status().isBadRequest()).andReturn();
        int responseStatus = mvcResult.getResponse().getStatus();
        String response = mvcResult.getResponse().getContentAsString();
        //THEN
        assertThat(responseStatus).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response).contains(wrongValidationMessage);
    }

    private MockHttpServletRequestBuilder postMethodWithWithObjectAsJSON(String endpoint, String objectFromJSON) {
        return post(endpoint)
                .contentType(MediaType.APPLICATION_JSON).content(objectFromJSON);
    }
}
