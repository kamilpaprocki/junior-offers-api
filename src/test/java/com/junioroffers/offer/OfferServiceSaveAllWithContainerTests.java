package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dao.Offer;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOffer;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("container")
public class OfferServiceSaveAllWithContainerTests implements SampleOffer, SampleOfferDto {

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_save_given_offers_to_database(@Autowired OfferService offerService) {
        //given
        List<OfferDto> offerDtos = Arrays.asList(firstOfferDtoWithoutId(), secondOfferDtoWithoutId());
        int numberOffersBeforeSave = offerService.getOffers().size();
        //when
        List<OfferDto> savedOffers = offerService.saveAll(offerDtos);
        int numberOffersAfterSave = offerService.getOffers().size();
        //then
        assertThat(numberOffersAfterSave).isGreaterThan(numberOffersBeforeSave);
        assertThat(savedOffers).containsAnyElementsOf(offerDtos);
    }
}
