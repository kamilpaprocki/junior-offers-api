package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOffer;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static com.mongodb.assertions.Assertions.assertNull;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("container")
public class OfferServiceWithContainerTests implements SampleOfferDto, SampleOffer {

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_return_all_offers(@Autowired OfferService offerService) {
        //given
        List<OfferDto> offers = Arrays.asList(firstOfferDtoWithId(), secondOfferDtoWithId());
        //when
        final List<OfferDto> offersFromDatabase = offerService.getOffers();
        //then
        assertThat(offersFromDatabase).containsAll(offers);
    }

    @Test
    void should_return_offer_with_given_id(@Autowired OfferService offerService) {
        //given
        OfferDto offerDto = firstOfferDtoWithId();
        String offerId = "629f77e9645fea56b0d31842";
        //when
        final OfferDto offerDtoFromDatabase = offerService.getOfferById(offerId);
        //then
        assertThat(offerDtoFromDatabase).isEqualTo(offerDto);
    }

    @Test
    void should_return_null_where_there_is_no_offer_with_given_id(@Autowired OfferService offerService) {
        //given
        String offerId = "500";
        //when
        OfferDto offerDto = offerService.getOfferById(offerId);
        //then
        assertNull(offerDto);
    }
}
