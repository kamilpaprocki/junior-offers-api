package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import java.time.Duration;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.junioroffers.config.RedisTestConfig.EXPOSED_REDIS_PORT;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("redistest")
public class OfferServiceRedisCacheExpireTestWithContainerTest {

    @Container
    private final static GenericContainer<?> REDIS_CONTAINER = new GenericContainer<>("redis");

    static {
        REDIS_CONTAINER.withExposedPorts(EXPOSED_REDIS_PORT);
        REDIS_CONTAINER.start();
        System.setProperty("spring.redis.port", String.valueOf(REDIS_CONTAINER.getFirstMappedPort()));
        System.setProperty("redis-url.port", String.valueOf(REDIS_CONTAINER.getFirstMappedPort()));
    }

    @MockBean
    private OfferRepository offerRepository;

    @Autowired
    private OfferService offerService;

    @Autowired
    CacheManager cacheManager;

    @Test
    public void should_invalidate_cache_automatically_after_given_time() {
        final String expectedCacheName = "offers";
        final int expectedTimesOfInvocation = 2;
        final Duration duration = Duration.ofSeconds(4);
        offerService.getOffers();
        AssertionsForClassTypes.assertThat(cacheManager.getCacheNames().contains(expectedCacheName)).isTrue();

        await()
                .atMost(duration)
                .untilAsserted(() -> {
                            offerService.getOffers();
                            verify(offerRepository, times(expectedTimesOfInvocation)).findAll();
                        }
                );
    }
}
