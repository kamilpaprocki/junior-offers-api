package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.infrastructure.offer.client.dto.JobOfferDto;
import com.junioroffers.offer.domain.dto.SampleJobOfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

@Testcontainers
@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("container")
public class OfferServiceSaveAllJobOffersWithContainerTest implements SampleJobOfferDto, SampleOfferDto {

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_save_offers_with_unique_url(@Autowired OfferService offerService, @Autowired OfferRepository offerRepository) {
        //given
        offerService.createOrUpdateOffer(offerDtoWithNotUniqueUrl());
        List<JobOfferDto> givenOffers = Arrays.asList(jobOfferDtoWithUniqueUrl(), jobOfferDtoWithNotUniqueUrl());
        int numberOffersBeforeSave = offerService.getOffers().size();
        then(offerRepository.existsByOfferUrl("IS_NOT_UNIQUE")).isTrue();
        //when
        List<JobOfferDto> savedOffers = offerService.saveAllJobOffers(givenOffers);
        int numberOffersAfterSave = offerService.getOffers().size();
        //then
        assertThat(offerRepository.existsByOfferUrl("IS_UNIQUE")).isTrue();
        assertThat(numberOffersAfterSave).isGreaterThan(numberOffersBeforeSave);
    }
}
