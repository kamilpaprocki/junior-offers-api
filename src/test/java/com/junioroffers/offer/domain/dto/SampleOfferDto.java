package com.junioroffers.offer.domain.dto;

public interface SampleOfferDto {

    default OfferDto firstOfferDtoWithId() {
        return OfferDto.builder()
                .id("629f77e9645fea56b0d31842")
                .companyName("HARMAN Connected Services")
                .jobPosition("Junior Java SE Developer for Automotive")
                .salary("7k - 10k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-java-se-developer-for-automotive-harman-connected-services-lodz-yafxatha")
                .build();
    }

    default OfferDto secondOfferDtoWithId() {
        return OfferDto.builder()
                .id("629f77e9645fea56b0d31843")
                .companyName("S2Innovation Sp. z o. o.")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-remote-java-developer-s2innovation-krakow-stddogtj")
                .build();
    }

    default OfferDto firstOfferDtoWithoutId() {
        return OfferDto.builder()
                .companyName("HARMAN Connected Services")
                .jobPosition("Junior Java SE Developer for Automotive")
                .salary("7k - 10k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-java-se-developer-for-automotive-harman-connected-services-lodz-yafxathaa")
                .build();
    }

    default OfferDto secondOfferDtoWithoutId() {
        return OfferDto.builder()
                .companyName("S2Innovation Sp. z o. o.")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-remote-java-developer-s2innovation-krakow-stddogtjj")
                .build();
    }

    default OfferDto offerDtoWithNotUniqueUrl() {
        return OfferDto.builder()
                .companyName("S2Innovation Sp. z o. o.")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("IS_NOT_UNIQUE")
                .build();
    }

    default OfferDto offerDtoWithUniqueUrl() {
        return OfferDto.builder()
                .companyName("S2Innovation Sp. z o. o.")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("IS_UNIQUE")
                .build();
    }

    default OfferDto offerDtoWithEmptyCompanyName() {
        return OfferDto.builder()
                .companyName("")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("IS_NOT_UNIQUE")
                .build();
    }
}
