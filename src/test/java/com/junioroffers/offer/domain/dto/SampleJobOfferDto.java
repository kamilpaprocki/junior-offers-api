package com.junioroffers.offer.domain.dto;

import com.junioroffers.infrastructure.offer.client.dto.JobOfferDto;

public interface SampleJobOfferDto {

    default JobOfferDto jobOfferDtoWithUniqueUrl() {
        return JobOfferDto.builder()
                .companyName("HARMAN Connected Services")
                .jobPosition("Junior Java SE Developer for Automotive")
                .salary("7k - 10k PLN")
                .offerUrl("IS_UNIQUE")
                .build();
    }

    default JobOfferDto jobOfferDtoWithNotUniqueUrl() {
        return JobOfferDto.builder()
                .companyName("S2Innovation Sp. z o. o.")
                .jobPosition("Junior Remote Java Developer")
                .salary("4k - 8k PLN")
                .offerUrl("IS_NOT_UNIQUE")
                .build();
    }
}
