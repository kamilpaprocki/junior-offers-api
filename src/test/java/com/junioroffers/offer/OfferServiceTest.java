package com.junioroffers.offer;

import com.junioroffers.config.TestData;
import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOffer;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferExistException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class OfferServiceTest implements SampleOffer, SampleOfferDto {

    OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
    OfferService offerService = new OfferService(offerRepository);

    @Test
    public void should_return_offer_list() {
        //given
        when(offerRepository.findAll()).thenReturn(Collections.singletonList(firstOfferWithId()));
        //when
        List<OfferDto> offers = offerService.getOffers();
        //then
        assertThat(offers.size()).isEqualTo(1);
    }

    @Test
    public void should_return_empty_list_when_there_is_no_offers() {
        //given
        when(offerRepository.findAll()).thenReturn(Collections.emptyList());
        //when
        List<OfferDto> offers = offerService.getOffers();
        //then
        assertThat(offers.size()).isEqualTo(0);
    }

    @Test
    public void should_return_offer_with_given_offer_id() {
        //given
        String offerId = TestData.FIRST_OFFER_ID;
        when(offerRepository.findById(offerId)).thenReturn(Optional.of(firstOfferWithId()));
        //when
        OfferDto offer = offerService.getOfferById(offerId);
        //then
        assertThat(offer).isInstanceOf(OfferDto.class).isNotNull();
    }

    @Test
    public void should_return_null_when_there_is_no_offer_with_given_id() {
        //given
        String wrongId = "fd4rdxgky";
        when(offerRepository.findById(wrongId)).thenReturn(Optional.empty());
        //when
        OfferDto offer = offerService.getOfferById(String.valueOf(ArgumentMatchers.anyLong()));
        //then
        assertThat(offer).isNull();
    }

    @Test
    public void should_return_1_when_offer_will_be_delete_with_given_id() {
        //given
        String offerId = TestData.FIRST_OFFER_ID;
        when(offerRepository.deleteOfferById(offerId)).thenReturn(1);
        //when
        int result = offerService.deleteOfferById(offerId);
        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void return_offer_with_all_fields_not_null_when_offer_is_updating() {
        //given
        when(offerRepository.save(firstOfferWithId())).thenReturn(firstOfferWithId());
        //when
        OfferDto offer = offerService.createOrUpdateOffer(OfferMapper.mapToOfferDto(firstOfferWithId()));
        //then
        assertThat(offer).isNotNull().hasNoNullFieldsOrProperties();
    }

    @Test
    public void return_offer_without_id_when_offer_is_creating() {
        //given
        when(offerRepository.save(returnFirstOfferWithoutId())).thenReturn(returnFirstOfferWithoutId());
        //when
        OfferDto offer = offerService.createOrUpdateOffer(OfferMapper.mapToOfferDto(returnFirstOfferWithoutId()));
        //then
        assertThat(offer).isNotNull().hasFieldOrPropertyWithValue("id", null);
    }

    @Test
    public void should_throw_OfferExistException_when_is_saving_offer_with_the_same_URL() {
        //given
        List<OfferDto> offerDtos = Arrays.asList(offerDtoWithNotUniqueUrl(), offerDtoWithUniqueUrl());
        when(offerService.createOrUpdateOffer(offerDtoWithNotUniqueUrl())).thenThrow(OfferExistException.class);
        //when //then
        Assertions.assertThrows(OfferExistException.class, () -> offerService.saveAll(offerDtos));
    }
}
