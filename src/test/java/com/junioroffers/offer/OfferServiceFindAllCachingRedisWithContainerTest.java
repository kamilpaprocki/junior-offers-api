package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.junioroffers.config.RedisTestConfig.EXPOSED_REDIS_PORT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Testcontainers
@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("redistest")
public class OfferServiceFindAllCachingRedisWithContainerTest {

    @MockBean
    OfferRepository offerRepository;

    @Autowired
    OfferService offerService;

    @Autowired
    CacheManager cacheManager;

    @Container
    private static final GenericContainer<?> REDIS_CONTAINER = new GenericContainer<>("redis");

    static {
        REDIS_CONTAINER.withExposedPorts(EXPOSED_REDIS_PORT);
        REDIS_CONTAINER.start();
        System.setProperty("spring.redis.port", String.valueOf(REDIS_CONTAINER.getFirstMappedPort()));
        System.setProperty("redis-url.port", String.valueOf(REDIS_CONTAINER.getFirstMappedPort()));
    }

    @Test
    public void should_using_caching_when_findAllOffers_is_using_twice() {
        //given
        String cacheName = "offers";
        assertThat(cacheManager.getCacheNames().isEmpty()).isTrue();
        int numberOfInvocation = 1;
        //when
        offerService.getOffers();
        offerService.getOffers();
        //then
        verify(offerRepository, times(numberOfInvocation)).findAll();
        assertThat(cacheManager.getCacheNames().contains(cacheName)).isTrue();
    }
}
