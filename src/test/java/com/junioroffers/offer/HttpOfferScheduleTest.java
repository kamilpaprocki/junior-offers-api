package com.junioroffers.offer;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.scheduling.HttpOfferSchedule;
import org.junit.jupiter.api.Test;
import org.mockito.verification.VerificationMode;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.Duration;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;


@SpringBootTest(classes = JobOffersApplication.class)
@ActiveProfiles("schedulertest")
public class HttpOfferScheduleTest {

    @SpyBean
    HttpOfferSchedule httpOfferSchedule;

    @Test
    public void should_add_new_offer_after_given_time() {
        //given
        Duration durationTime = Duration.ofSeconds(10);
        VerificationMode schedulingTimes = atLeast(2);
        //when //then
        await()
                .atMost(durationTime)
                .untilAsserted(() -> verify(httpOfferSchedule, schedulingTimes).getOffers());
    }
}
