package com.junioroffers.config;

import com.junioroffers.offer.OfferController;
import com.junioroffers.offer.OfferRepository;
import com.junioroffers.offer.OfferService;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.OfferExistException;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;

public class MockMvcConfig implements SampleOfferDto {

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = mock(OfferRepository.class);
        return new OfferService(offerRepository) {

            @Override
            public List<OfferDto> getOffers() {
                return Arrays.asList(firstOfferDtoWithId(), secondOfferDtoWithId());
            }

            @Override
            public OfferDto getOfferById(String id) {
                if (TestData.FIRST_OFFER_ID.equals(id)) {
                    return firstOfferDtoWithId();
                }
                if (TestData.SECOND_OFFER_ID.equals(id)) {
                    return secondOfferDtoWithId();
                }
                return null;
            }

            @Override
            public OfferDto createOrUpdateOffer(OfferDto offerDto) {
                if (offerDto.getId() != null) {
                    return firstOfferDtoWithId();
                }
                if (offerDto.equals(offerDtoWithNotUniqueUrl()))
                    throw new OfferExistException("There is offer with given URL: " + offerDto.getOfferUrl());
                return offerDto;
            }

            @Override
            public int deleteOfferById(String id) {
                if (TestData.FIRST_OFFER_ID.equals(id)) {
                    return 1;
                }
                return 0;
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService) {
        return new OfferController(offerService);
    }

    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler() {
        return new OfferControllerErrorHandler();
    }
}
