package com.junioroffers.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "redis-url")
public class RedisUrlConfig {

    private String hostname;
    private int port;
}
