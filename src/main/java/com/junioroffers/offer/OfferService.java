package com.junioroffers.offer;

import com.junioroffers.infrastructure.offer.client.dto.JobOfferDto;
import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.dao.Offer;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferExistException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfferService {

    private final OfferRepository offerRepository;

    @Cacheable(value = "offers")
    public List<OfferDto> getOffers() {
        return offerRepository.findAll()
                .stream()
                .map(OfferMapper::mapToOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto getOfferById(String id) {
        return OfferMapper.mapToOfferDto(offerRepository.findById(id).orElse(null));
    }

    public OfferDto createOrUpdateOffer(OfferDto offerDto) {
        try {
            offerRepository.save(OfferMapper.mapToOfferDao(offerDto));
        } catch (DuplicateKeyException e) {
            throw new OfferExistException("There is offer with given URL: " + offerDto.getOfferUrl());
        }
        return offerDto;
    }

    public List<OfferDto> saveAll(List<OfferDto> offerDtos) {
        offerDtos.forEach(this::createOrUpdateOffer);
        return offerDtos;
    }

    public int deleteOfferById(String id) {
        return offerRepository.deleteOfferById(id);
    }

    public List<JobOfferDto> saveAllJobOffers(List<JobOfferDto> jobOffers) {
        List<Offer> offers = jobOffers.stream()
                .filter((o -> !offerRepository.existsByOfferUrl(o.getOfferUrl())))
                .map(OfferMapper::mapToOfferDao)
                .collect(Collectors.toList());
        offerRepository.saveAll(offers);
        return offers.stream()
                .map(OfferMapper::mapToJobOfferDto)
                .collect(Collectors.toList());
    }
}
