package com.junioroffers.offer.domain.exceptions;

import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class OfferErrorResponse {

    private String message;
    private String timestamp;
    private HttpStatus httpStatus;
}
