package com.junioroffers.offer.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class OfferControllerErrorHandler {

    @ExceptionHandler(value = OfferNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    protected OfferErrorResponse handleOfferNotFoundException(OfferNotFoundException e) {
        return new OfferErrorResponse(e.getMessage(), returnNewDate(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected OfferErrorValidationResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e){
        List<String> validationMessages = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(errorMessage -> validationMessages.add(errorMessage.getDefaultMessage()));
        return new OfferErrorValidationResponse(validationMessages, returnNewDate(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OfferExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    protected OfferErrorResponse handleOfferExistException(OfferExistException e){
        return new OfferErrorResponse(e.getMessage(), returnNewDate(), HttpStatus.CONFLICT);
    }

    private String returnNewDate() {
        return LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));
    }
}
