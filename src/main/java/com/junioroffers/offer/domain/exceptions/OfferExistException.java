package com.junioroffers.offer.domain.exceptions;

public class OfferExistException extends RuntimeException {

    public OfferExistException(String message) {
        super(message);
    }
}
