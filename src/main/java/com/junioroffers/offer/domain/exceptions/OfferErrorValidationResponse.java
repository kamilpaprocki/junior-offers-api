package com.junioroffers.offer.domain.exceptions;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.List;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class OfferErrorValidationResponse {

        private List<String> message;
        private String timestamp;
        private HttpStatus httpStatus;

}
