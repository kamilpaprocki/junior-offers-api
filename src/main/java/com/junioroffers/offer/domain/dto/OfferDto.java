package com.junioroffers.offer.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
public class OfferDto implements Serializable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("companyName")
    @NotNull(message = "Company name cannot be null.")
    @NotEmpty(message = "Company name cannot be empty.")
    private String companyName;
    @JsonProperty("jobPosition")
    @NotNull(message = "Job position cannot be null.")
    @NotEmpty(message = "Job position cannot be empty.")
    private String jobPosition;
    @JsonProperty("salary")
    @NotNull(message = "Salary cannot be null.")
    @NotEmpty(message = "Salary cannot be empty.")
    private String salary;
    @JsonProperty("offerUrl")
    @NotNull(message = "Offer url cannot be null.")
    @NotEmpty(message = "Offer url cannot be empty.")
    private String offerUrl;
}
