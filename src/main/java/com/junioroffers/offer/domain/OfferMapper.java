package com.junioroffers.offer.domain;

import com.junioroffers.infrastructure.offer.client.dto.JobOfferDto;
import com.junioroffers.offer.domain.dao.Offer;
import com.junioroffers.offer.domain.dto.OfferDto;

public class OfferMapper {

    public static OfferDto mapToOfferDto(Offer offer) {
        if (offer == null) {
            return null;
        }
        return OfferDto.builder()
                .id(offer.getId())
                .companyName(offer.getCompanyName())
                .jobPosition(offer.getJobPosition())
                .salary(offer.getSalary())
                .offerUrl(offer.getOfferUrl())
                .build();
    }

    public static Offer mapToOfferDao(OfferDto offerDto) {
        if (offerDto == null) {
            return null;
        }
        return Offer.builder()
                .id(offerDto.getId())
                .companyName(offerDto.getCompanyName())
                .jobPosition(offerDto.getJobPosition())
                .salary(offerDto.getSalary())
                .offerUrl(offerDto.getOfferUrl())
                .build();
    }

    public static Offer mapToOfferDao(JobOfferDto offerDto) {
        if (offerDto == null) {
           throw new IllegalStateException("JobOfferDto cannot be a null.");
        }
        return Offer.builder()
                .id(offerDto.getId())
                .companyName(offerDto.getCompanyName())
                .jobPosition(offerDto.getJobPosition())
                .salary(offerDto.getSalary())
                .offerUrl(offerDto.getOfferUrl())
                .build();
    }

    public static JobOfferDto mapToJobOfferDto(Offer offer) {
        if (offer == null) {
            return null;
        }
        return JobOfferDto.builder()
                .id(offer.getId())
                .companyName(offer.getCompanyName())
                .jobPosition(offer.getJobPosition())
                .salary(offer.getSalary())
                .offerUrl(offer.getOfferUrl())
                .build();
    }
}
