package com.junioroffers.offer.domain.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document("juniorOffers")
public class Offer {

    @Id
    private String id;

    @Field("company_name")
    private String companyName;

    @Field("job_position")
    private String jobPosition;

    @Field("salary")
    private String salary;

    @Field("offer_url")
    @Indexed(unique = true)
    private String offerUrl;
}
