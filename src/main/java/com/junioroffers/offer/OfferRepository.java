package com.junioroffers.offer;

import com.junioroffers.offer.domain.dao.Offer;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface OfferRepository extends Repository<Offer, Long> {

    int deleteOfferById(String id);

    Optional<Offer> findById(String id);

    List<Offer> findAll();

    Offer save(Offer offer);

    default List<Offer> saveAll(List<Offer> offers) {
        offers.forEach(this::save);
        return offers;
    }

    boolean existsByOfferUrl(String url);
}
