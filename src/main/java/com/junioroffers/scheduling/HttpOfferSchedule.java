package com.junioroffers.scheduling;

import com.junioroffers.infrastructure.offer.client.OfferClient;
import com.junioroffers.infrastructure.offer.client.dto.JobOfferDto;
import com.junioroffers.offer.OfferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class HttpOfferSchedule {

    private final OfferClient offerClient;
    private final OfferService offerService;

    @Scheduled(fixedDelayString = "${http-offers-scheduler.delay}")
    public void getOffers() {
        final List<JobOfferDto> offerDtos = offerClient.getOffers();
        log.info("Return {} offers.", offerDtos.size());
        final List<JobOfferDto> saveOffers = offerService.saveAllJobOffers(offerDtos);
        log.debug("Saved {} new offers.", saveOffers.size());
    }
}
